import Vue from 'vue';
import App from './App';
// import store from './store'
// Vue.prototype.$store = store

// import share1 from './utils/share.js';
import filters from './utils/filter.js';
import nodata from './pages/common/nodata.vue';
import header from './pages/common/header.vue';
import bButton from './pages/common/button.vue';
import tabs from "./pages/common/tabs.vue";
Vue.config.productionTip = false
// Vue.mixin(share1)
App.mpType = 'app'
import global from './utils/global.js';
// import roles from './utils/roles.js';
// Vue.prototype.getImg = global.getImg;
// Vue.prototype.roles = roles;
Vue.prototype.copy = global.copy;
Vue.prototype.setCopy = global.setCopy;
Vue.prototype.go = global.go;
Vue.prototype.back = global.back;
Vue.prototype.$toast = global.toast;
// Vue.prototype.openApp = global.openApp;
Vue.prototype.callPhone = global.callPhone;
Vue.prototype.h5Reload = global.h5Reload;
// Vue.prototype.getUser = global.getUser;
Vue.prototype.previewImg = global.previewImg;
// Vue.prototype.getBanner = global.getBanner;
// Vue.prototype.loginOut = global.loginOut;
// Vue.prototype.getConfig = global.getConfig;
Vue.prototype.getStorage = global.getStorage;
// Vue.prototype.getLocation = global.getLocation;
// Vue.prototype.hasRoles = global.hasRoles;
// Vue.prototype.scanAudio = global.scanAudio;
// Vue.prototype.verification = global.verification;
// Vue.component('tabBar',tabbar);
Vue.component('noData',nodata);
// Vue.component('poster',poster);
Vue.component('uniHeader',header);
Vue.component('bButton',bButton);
// Vue.component('model',model);
Vue.component('tabs',tabs);
//过滤器统一处理加载
Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})
const app = new Vue({
	// store,
	...App
})
app.$mount()
