// 子模块moduleA路径：store/modules/moduleA.js 
export default {
	state: {
		time: 0,
		roles: [],
		user:'',
	},
	getters: {

	},
	mutations: {
		uploadUser(state,payload) {
			state.user = payload;
		},
		uploadRoles(state,payload) {
			state.roles = payload;
		},
		updateTime(state) { //更新当前时间戳
			state.time = Date.now()
		}
	},
	actions: {
		uploadUser(context,payload) {
			context.commit('uploadUser',payload)
		},
		uploadRoles(context,payload) {
			context.commit('uploadRoles',payload)
		},
		updateTime (context , payload) {
		    context.commit('updateTime',payload)
		}
	}
}
