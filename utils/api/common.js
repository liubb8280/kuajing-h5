export default {
	'codeApi': 'common/sendCode',
	'imgCodeApi': 'common/captcha',
	'bannerApi': 'member/banner/getList',//轮播图列表
	'indexStyle': 'common/floor/getList',//首页装修配置
	'configApi': 'common/config/getList',//获取配置
	'brandList': 'member/brand/getList',//品牌选择器
	'searchAdressApi': 'member/txAddress/get',//地图关键词搜索
	'locationToAdressApi': 'member/map/geocoder',//地址转换
	'todoListApi': 'member/todo/getList',//待办列表
	'todoNumApi': 'member/todo/count',//统计未读数量
	'todoListApi1': 'member/todo/getList2',//待办列表
	'todoNumApi1': 'member/todo/count2',//统计未读数量
	'todoReadApi': 'member/todo/read',//待办已读
	'getOrderIdApi': 'member/adOrderCommon/getBusinessIdByOrderNo' // 根据类型以及业务类型 非必要的店铺IP查询 详情页面DI
}