export default {
	'getUserApi': 'member/getUser',
	'getRoleListApi': 'member/getAllRole',//获取当前所有权限
	'changeRoleApi': 'member/changeRole',//切换当前角色
}