import Vue from 'vue';
const API = require('./api/user.js').default;
const ApiConmon = require('./api/common.js').default;
const $ = require('./api.js');
export default {
	getImg() {
		return 'https://api.heylie.cn/api/img?xq=paqukun'
	},
	hasRoles(name) {
		let list = this.$store.state.common.roles ? this.$store.state.common.roles : [];
		// console.log(list,name);
		if(list.indexOf(name) >= 0 || name == '') {
			return true
		}else{
			return false
		}
	},
	getStorage(dom) {
		if(!dom){return};
		if(dom == 'top') {
			return uni.getStorageSync('bartop') ? uni.getStorageSync('bartop') : 0;
		}else if(dom == 'iphonex') {
			return 	uni.getStorageSync('isIphonex') ? uni.getStorageSync('isIphonex') : false;
		}else{
			return 	uni.getStorageSync(dom) ? uni.getStorageSync(dom) : '';
		}
	},
	getConfig(options) {
		let config = uni.getStorageSync('config') ? uni.getStorageSync('config') : '';
		if(options.isAjax || config == '') {
			$.ajax({
				url: ApiConmon.configApi,
				data: {},
				method: 'GET',
				success(res) {
					config = res.data ? res.data : '';
					uni.setStorageSync('config',config);
					if(options.success) {
						options.success(config);
					}
				}
			})
		}else{
			if(options.success) {
				options.success(config);
			}
		}
	},
	loginOut() {
		uni.removeStorageSync('token');
		uni.removeStorageSync('userInfo');
		$.go('/pages/passport/login');
	},
	previewImg(url,urls) {
		let video = ['mp4','MOV'];
		let file = ['pdf'];
		if(url.indexOf(video) >= 0) {
			this.go('/pages/index/video?url='+url);
			return;
		}
		if(url.indexOf(file) >= 0) {
			// #ifdef H5
			window.location.href = url;
			// #endif
			// #ifdef APP-PLUS
			plus.runtime.openURL(url, function(res) {});  
			// #endif
			return;
		}
		if(!urls){
			urls = [url];
		}
		uni.previewImage({
			current: url,
			urls: urls
		})
	},
	getBanner(options) {
		$.ajax({
			url: ApiConmon.bannerApi,
			isAuth: true,
			data: {location: options.type ? options.type: 1},
			method: 'GET',
			success(res) {
				if(options.success) {
					options.success(res);
				}
			}
		})
	},
	getLocation(options) {
		let location = uni.getStorageSync('location') ? uni.getStorageSync('location') : '';
		if(!options || options.isAjax || location == '') {
			$.getLocation({
				success(res) {
					$.ajax({
						url: ApiConmon.locationToAdressApi,
						data: {
							key: getApp().globalData.mapKey,
							location: res.latitude+','+res.longitude
						},
						method: 'GET',
						success(res1) {
							let adInfo = res1.data.result.ad_info;
							uni.setStorageSync('location',adInfo);
							if(options && options.success) {
								options.success(adInfo);
							}
						}
					})
				}
			})
		}else{
			if(options.success) {
				options.success(location);
			}
		}
	},
	getUser(options) {
		const self  =this;
		let user = uni.getStorageSync('userInfo') ? uni.getStorageSync('userInfo') : '';
		if(options.isAjax || user == '') {
			$.ajax({
				url: API.getUserApi,
				data: {},
				method: 'GET',
				success(res) {
					uni.removeStorageSync('userInfo');
					user = res.data ? res.data : '';
					uni.setStorageSync('userInfo',user);
					if(options.success) {
						options.success(user);
					}
					self.$store.dispatch('uploadUser',user);
					self.$store.dispatch('uploadRoles',user.permissions);
				}
			})
		}else{
			if(options.success) {
				options.success(user);
			}
			self.$store.dispatch('uploadUser',user);
			self.$store.dispatch('uploadRoles',user.permissions);
		}
		
	},
	h5Reload() {
		var url = window.location.href; //页面url
		var beforeUrl = url.substr(0, url.indexOf("?"));
		var afterUrl = url.substr(url.indexOf("#"));
		window.location.replace(beforeUrl + afterUrl);
	},
	callPhone(phone) {
		const self = this;
		if(!phone || phone == ''){return}
		uni.makePhoneCall({
			phoneNumber: phone
		})
	},
	copy(text) {
		$.uniCopy({
			content: text,
			success: (res) => {
				uni.showToast({
					title: res,
					icon: 'none'
				})
			},
			error: (e) => {
				uni.showToast({
					title: e,
					icon: 'none',
					duration: 3000,
				})
			}
		})
	},
	setCopy(options) {
		// #ifdef H5
		$.setCopy().then((res) => {
			console.log(res);
			if(options.success) {
				options.success(res);
			}
		})
		// #endif
		// #ifndef H5
		uni.getClipboardData({
		    success: function (res) {
				uni.hideToast();
				if(options.success) {
					options.success(res.data);
				}
		    }
		});
		// #endif
		
	},
	openApp(type) {
		let pname = '';
		let action = '';
		if(type == 'wx') {
			pname = "com.tencent.mm";
			action = 'weixin://';
		}
		if(type == 'tb') {
			pname = 'com.taobao.taobao';
			action = 'taobao://';
		}
		if(type == 'jd') {
			pname = 'com.jingdong.app.mall';
			action = 'openApp.jdMobile://' ;
		}
		if(type == 'pdd') {
			pname = 'com.xunmeng.pinduoduo';
			action = 'pinduoduo://';
		}
		if(type == 'dy') {
			pname = 'com.ss.android.ugc.aweme';
			action = 'snssdk1128://';
		}
		if(type == 'alibaba') {
			pname = 'com.alibaba.wireless';
			action = 'wireless1688://';
		}
		// 判断平台
		if (plus.os.name == 'Android') {
			plus.runtime.launchApplication({
					pname: pname
				},
				function(e) {
					console.log('Open system default browser failed: ' + e.message);
				}
			);
		} else if (plus.os.name == 'iOS') {
			plus.runtime.launchApplication({
				action: action
			}, function(e) {
				console.log('Open system default browser failed: ' + e.message);
			});
		}
	},
	
	openWX() {
		// 判断平台  
		if (plus.os.name == 'Android') {
			plus.runtime.launchApplication({
					pname: 'com.tencent.mm'
				},
				function(e) {
					console.log('Open system default browser failed: ' + e.message);
				}
			);
		} else if (plus.os.name == 'iOS') {
			plus.runtime.launchApplication({
				action: 'weixin://'
			}, function(e) {
				console.log('Open system default browser failed: ' + e.message);
			});
		}

	},
	toast(tip, icon, time) {
		// #ifdef APP-PLUS
		plus.nativeUI.setUIStyle('light'); 
		// #endif
		uni.showToast({
			title: tip,
			icon: icon ? icon : 'none',
			duration: time ? time : 2000
		})
	},
	go(url, type = 1, time) {
		$.go(url, type, time);
	},
	back(type,time) {
		let pages = getCurrentPages();
		if(pages.length <= 1) {
			uni.reLaunch({
				url: '/pages/index/detail',
			})
			return
		}
		if (time) {
			setTimeout(() => {
				uni.navigateBack({
					delta: type ? type : 1
				})
			}, time);
		} else {
			uni.navigateBack({
				delta: type ? type : 1
			})
		}
	},
	scanAudio() {
		let music = null;
		music = uni.createInnerAudioContext()
		music.src = '../../static/ding.mp3'
		music.pause()
		music.play()
		music.onEnded(() => {
			music = null
		})
	},
	verification(text) {
		let { realname } = uni.getStorageSync('userInfo')
		if(realname.indexOf(text) == 0) {
			return true
		} else {
			return false
		}
	}
};
