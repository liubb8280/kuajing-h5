// 权限列表
module.exports = {
	'userRepairsBtn': 'duty:maintain:addOrder',//维修下单
	'workRepairsBtn': 'duty:maintain:orderControl',//维修下单工作台
	'userAd': 'duty:ad',//广宣下单
	'userAdBtn': 'duty:ad:addOrder',//广宣下单
	'userAdGroupBtn': 'duty:ad:addGroupOrder',//广宣团单
	'workAdGroupBtn': 'duty:ad:groupOrder',//广宣我参与的团购订单
	'userTransportBtn': 'duty:transport:addOrder',//运输下单
	'userTransportSingleBtn': 'duty:transport:addOrder:single',//单目的地下单
	'userTransportMoreBtn': 'duty:transport:addOrder:more',//多目的地下单
	'userTransportAloneBtn': 'duty:transport:addOrder:alone',//独立商户下单
	'userTransportFollowBtn': 'duty:transport:addOrder:follow',//随车下单
	'userReplaceBtn': 'duty:waitingPost:addOrder',//代岗下单
	'workReplaceBtn': 'duty:waitingPost:orderControl',//代岗工作台
	'workReplaceLeaderBtn': 'duty:waitingPost:orderControl:leader',//代岗接单控制台
	'userTalentsBtn': 'duty:job:addOrder',//人才招聘
	'userItBtn': 'duty:itOperation:addOrder',//IT运维
	'workProjectBtn': 'duty:project:orderControl',//项目工程
	'tradeSignBtn': 'trade:sign:addOrder',//贸易下单
	'tradeAloneBtn': 'trade:alone:addOrder',//独立商户贸易下单
	'tradeSpecialBtn': 'trade:specialGoods:addOrder',//特殊采购
	'workTransportBtn': 'duty:transport:orderControl',//运输工作
	'workTransportMoreBtn': 'duty:transport:orderControl:more',//用车订单
	'workTransportAloneBtn': 'duty:transport:orderControl:alone',//独立商户订单
	'workTransportFollowBtn': 'duty:transport:orderControl:follow',//随车订单
	'workTransportWzpBtn': 'duty:transport:orderControl:wzp',//  指派按钮
	'workTradeSign': 'trade:alone:orderControl',//贸易独立商户工作台
	'workTradeSignSure': 'trade:alone:orderControl:sure', // 贸易独立商户工作台接单
	'workTradeSignCancel': 'trade:alone:orderControl:cancel', // 贸易独立商户工作台取消订单
	'transportOrderReassign': 'duty:transport:orderControl:reassign', // 审核转单  包含同意转单和取消转单
	'maintainCharge':'duty:maintain:orderControl:charge',
}